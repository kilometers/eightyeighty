#include <gtk/gtk.h>

#include "eightyeighty.h"
#include "window.h"
#include "prefs.h"

struct _EightyEightyPrefs {
    GtkDialog parent;
};

typedef struct _EightyEightyPrefsPrivate EightyEightyPrefsPrivate;

struct _EightyEightyPrefsPrivate {
    GSettings *settings;
};

G_DEFINE_TYPE_WITH_PRIVATE(EightyEightyPrefs, eightyeighty_prefs,
                           GTK_TYPE_DIALOG);

static void eightyeighty_prefs_init(EightyEightyPrefs *prefs) {
    EightyEightyPrefsPrivate *priv;

    priv = eightyeighty_prefs_get_instance_private(prefs);
    gtk_widget_init_template(GTK_WIDGET (prefs));
    priv->settings = g_settings_new(EIGHTYEIGHTY_APP_ID);
}

static void example_app_prefs_dispose(GObject *object) {
    EightyEightyPrefsPrivate *priv;

    priv = eightyeighty_prefs_get_instance_private(EIGHTYEIGHTY_PREFS (object));
    g_clear_object(&priv->settings);

    G_OBJECT_CLASS (eightyeighty_prefs_parent_class)->dispose(object);
}

static void eightyeighty_prefs_class_init(EightyEightyPrefsClass *class) {
    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS (class),
                                        "/sh/meters/EightyEighty/ui/prefs.ui");
}

EightyEightyPrefs * eightyeighty_prefs_new(EightyEightyAppWindow *win) {
    return g_object_new (EIGHTYEIGHTY_PREFS_TYPE, "transient-for", win,
                         "use-header-bar", TRUE, NULL);
}
