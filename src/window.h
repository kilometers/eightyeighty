#ifndef __EIGHTYEIGHTYAPPWIN_H
#define __EIGHTYEIGHTYAPPWIN_H

#include <gtk/gtk.h>
#include "eightyeighty.h"

#define EIGHTYEIGHTY_APP_WINDOW_TYPE (eightyeighty_app_window_get_type ())
G_DECLARE_FINAL_TYPE (EightyEightyAppWindow, eightyeighty_app_window, EIGHTYEIGHTY, APP_WINDOW, GtkApplicationWindow)

EightyEightyAppWindow *eightyeighty_app_window_new (EightyEightyApp *app);
void eightyeighty_app_window_open (EightyEightyAppWindow *win);

#endif 
