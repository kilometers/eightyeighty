#ifndef __EIGHTYEIGHTYAPP_H
#define __EIGHTYEIGHTYAPP_H

#include <gtk/gtk.h>

#define EIGHTYEIGHTY_APP_ID "sh.meters.EightyEighty"

#define EIGHTYEIGHTY_APP_TYPE (eightyeighty_app_get_type ())
G_DECLARE_FINAL_TYPE (EightyEightyApp, eightyeighty_app, EIGHTYEIGHTY, APP, GtkApplication)

EightyEightyApp *eightyeighty_app_new(void);

#endif