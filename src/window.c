#include <gtk/gtk.h>

#include "eightyeighty.h"
#include "window.h"

struct _EightyEightyAppWindow {
    GtkApplicationWindow parent;
};

typedef struct EightyEightyAppWindowPrivate EightyEightyAppWindowPrivate;

struct EightyEightyAppWindowPrivate {
    GtkWidget *glarea;
    GtkWidget *headebar;
};

G_DEFINE_TYPE_WITH_PRIVATE(EightyEightyAppWindow, eightyeighty_app_window,
                           GTK_TYPE_APPLICATION_WINDOW);

static void eightyeighty_app_window_init(EightyEightyAppWindow *win) {
    EightyEightyAppWindowPrivate *priv;

    priv = eightyeighty_app_window_get_instance_private(win);
    gtk_widget_init_template(GTK_WIDGET (win));
}

static void eightyeighty_app_window_dispose (GObject *object) {
    EightyEightyAppWindow *win;
    EightyEightyAppWindowPrivate *priv;

    win = EIGHTYEIGHTY_APP_WINDOW (object);
    priv = eightyeighty_app_window_get_instance_private(win);

    g_clear_object(&priv->glarea);

    G_OBJECT_CLASS (eightyeighty_app_window_parent_class)->dispose(object);
}

static void eightyeighty_app_window_class_init(
    EightyEightyAppWindowClass *class
) {
    G_OBJECT_CLASS (class)->dispose = eightyeighty_app_window_dispose;

    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS (class),
                                        "/sh/meters/EightyEighty/ui/window.ui");

    gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
                                                 EightyEightyAppWindow,
                                                 glarea);
}

EightyEightyAppWindow * eightyeighty_app_window_new(EightyEightyApp *app) {
    return g_object_new (EIGHTYEIGHTY_APP_WINDOW_TYPE,
                          "application", app,NULL);
}

void eightyeighty_app_window_update_glarea(EightyEightyAppWindow *win) {

}

void eightyeighty_app_window_open(EightyEightyAppWindow *win) {}
