#ifndef __EIGHTYEIGHTYPREFS_H
#define __EIGHTYEIGHTYPREFS_H

#include <gtk/gtk.h>
#include "window.h"

#define EIGHTYEIGHTY_PREFS_TYPE (eightyeighty_prefs_get_type())
G_DECLARE_FINAL_TYPE (EightyEightyPrefs, eightyeighty_prefs, EIGHTYEIGHTY, PREFS, GtkDialog)

EightyEightyPrefs * eightyeighty_prefs_new (EightyEightyAppWindow *win);

#endif 
