#include<stdint.h>

#include"emulator.h"

// input keys
#define LEFT 0x1
#define RIGHT 0x2
#define SHOOT 0x3
#define INSERT_COIN 0x4

uint8_t machine_in(State8080 *state, uint8_t port);
void machine_out(State8080 *state, uint8_t port);

void machine_key_down(uint8_t key);