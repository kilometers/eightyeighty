#pragma once
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <gio/gio.h>

typedef struct ConditionCodes {    
    uint8_t    z:1;    
    uint8_t    s:1;    
    uint8_t    p:1;    
    uint8_t    cy:1;    
    uint8_t    ac:1;    
    uint8_t    pad:3;    
} ConditionCodes;

typedef struct State8080 {
    uint8_t    a;
    uint8_t    b;
    uint8_t    c;
    uint8_t    d;
    uint8_t    e;
    uint8_t    h;
    uint8_t    l;
    uint16_t   sp;
    uint16_t   pc;
    uint8_t    *memory;
    struct     ConditionCodes cc;
    uint8_t    int_enable;
} State8080;

void unimplemented_instruction(State8080* state);
bool emulate_8080_op(State8080* state);
void sleep_cycles(int cycles);
void logic_flags_a(State8080 *state);
int parity(int x, int size);
void load_rom_into_memory(State8080 *state, char *fname);
void emulate_8080_rom_async(gpointer filename);
State8080* Init8080();
