#include <gtk/gtk.h>

#include "eightyeighty.h"
#include "prefs.h"
#include "window.h"

#include "emulator.h"

struct _EightyEightyApp {
    GtkApplication parent;
};

G_DEFINE_TYPE(EightyEightyApp, eightyeighty_app, GTK_TYPE_APPLICATION);

static void eightyeighty_app_init(EightyEightyApp *app) {}

static void open_file_activated(
    GSimpleAction *action,
    GVariant *parameter,
    gpointer app
) {
    GtkWindow *win = gtk_application_get_active_window(GTK_APPLICATION (app));

    GtkFileChooserNative *native;

    native = gtk_file_chooser_native_new ("Open File",
                                          GTK_WINDOW (win),
                                          GTK_FILE_CHOOSER_ACTION_OPEN,
                                          "_Open",
                                          "_Cancel");

    gint res = gtk_native_dialog_run(GTK_NATIVE_DIALOG (native));
    if (res == GTK_RESPONSE_ACCEPT) {
        GtkFileChooser *chooser = GTK_FILE_CHOOSER (native);
        char *filename = gtk_file_chooser_get_filename(chooser);

        GtkWidget *headerbar = gtk_window_get_titlebar(GTK_WINDOW (win));
        gtk_header_bar_set_subtitle(GTK_HEADER_BAR (headerbar), filename);

        g_free(filename);
    }
}

static void preferences_activated(
    GSimpleAction *action,
    GVariant *parameter,
    gpointer app
) {
    GtkWindow *win = gtk_application_get_active_window(GTK_APPLICATION (app));
    EightyEightyPrefs *prefs = eightyeighty_prefs_new(EIGHTYEIGHTY_APP_WINDOW (win));
    gtk_window_present(GTK_WINDOW (prefs));
}

static void about_dialog_activated(
    GSimpleAction *action,
    GVariant *parameter,
    gpointer app
) {
    GtkWindow *win = gtk_application_get_active_window(GTK_APPLICATION (app));
    GtkAboutDialog *about = GTK_ABOUT_DIALOG (gtk_about_dialog_new());

    const gchar *authors[] = {"kilometers", NULL};

    gtk_show_about_dialog (win,
                           "program-name", "EightyEighty",
                           "authors", authors,
                           "comments", "An Intel 8080 emulator",
                           "website", "https://meters.sh/EightyEighty",
                           "copyright", "Copyright kilometers 2018",
                           "version", "0.0.1",
                           NULL);
}

static void quit_activated(
    GSimpleAction *action,
    GVariant *parameter,
    gpointer app) {
    g_application_quit(G_APPLICATION (app));
}

static GActionEntry app_entries[] = {
    { "open_file", open_file_activated, NULL, NULL, NULL },
    { "preferences", preferences_activated, NULL, NULL, NULL },
    { "about", about_dialog_activated, NULL, NULL, NULL },
    { "quit", quit_activated, NULL, NULL, NULL }
};

static void eightyeighty_app_startup(GApplication *app) {
    const gchar *quit_accels[2] = { "<Ctrl>Q", NULL };
    const gchar *open_accels[2] = { "<Ctrl>O", NULL };

    G_APPLICATION_CLASS (eightyeighty_app_parent_class)->startup(app);

    g_action_map_add_action_entries(G_ACTION_MAP (app),
                                    app_entries, G_N_ELEMENTS (app_entries),
                                    app);
    gtk_application_set_accels_for_action(GTK_APPLICATION (app),
                                          "app.quit",
                                          quit_accels);
    gtk_application_set_accels_for_action(GTK_APPLICATION (app),
                                          "app.open_file",
                                          open_accels);
}

static void eightyeighty_app_activate(GApplication *app) {
    EightyEightyAppWindow *win = eightyeighty_app_window_new(EIGHTYEIGHTY_APP (app));
    gtk_window_present(GTK_WINDOW (win));
}

static void eightyeighty_app_open(
    GApplication *app,
    GFile **files,
    gint n_files,
    const gchar *hint
) {
    GList *windows;
    EightyEightyAppWindow *win;

    windows = gtk_application_get_windows(GTK_APPLICATION (app));
    if (windows)
        win = EIGHTYEIGHTY_APP_WINDOW (windows->data);
    else
        win = eightyeighty_app_window_new(EIGHTYEIGHTY_APP (app));
    
    eightyeighty_app_window_open(win);

    gtk_window_present(GTK_WINDOW (win));
}

static void eightyeighty_app_class_init (EightyEightyAppClass *class) {
    G_APPLICATION_CLASS (class)->startup = eightyeighty_app_startup;
    G_APPLICATION_CLASS (class)->activate = eightyeighty_app_activate;
    G_APPLICATION_CLASS (class)->open = eightyeighty_app_open;
}

EightyEightyApp * eightyeighty_app_new() {
    return g_object_new (EIGHTYEIGHTY_APP_TYPE,
                         "application-id", EIGHTYEIGHTY_APP_ID,
                         "flags", G_APPLICATION_HANDLES_OPEN,
                         NULL);
}
