#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <gio/gio.h>

#include "emulator.h"
#include "disassembler.h"
#include "io.h"

void emulate_8080_rom_async(gpointer data) {
    State8080 *state = Init8080();

    char *filename = "/hdd/Projects/EightyEighty/res/invaders";

    load_rom_into_memory(state, filename);

    bool not_done = true;

    while (not_done) {
        not_done = emulate_8080_op(state);
    }
}

void load_rom_into_memory(State8080 *state, char *filename) {
    FILE *f = fopen(filename, "rb");
    if (f == NULL) {
        g_printerr("error: Couldn't open %s\n", filename);
        exit(1);
    }
    fseek(f, 0L, SEEK_END);
    int fsize = ftell(f);
    fseek(f, 0L, SEEK_SET);

    uint8_t *buffer = &state->memory[0];
    fread(buffer, fsize, 1, f);
    fclose(f);
}

State8080* Init8080() {
    State8080 *state = calloc(1, sizeof(State8080));
    state->memory = malloc(0x10000); //16K
    return state;
}

int parity(int x, int size) {
    int i;
    int p = 0;
    x = (x & ((1 << size)-1));
    for (i = 0; i < size; i++) {
        if (x & 0x1) {
            p++;
            x = x >> 1;
        }
    }
    return (0 == (p & 0x1));
}

// the Intel 8080 runs at 2MHz stock, so each cycle should wait
// 500 nanoseconds.
void sleep_cycles(int cycles) {
    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = (500 * cycles);

    nanosleep(&tim, &tim2);
}

void logic_flags_a(State8080 *state) {
    state->cc.cy = state->cc.ac = 0;
    state->cc.z = (state-> a == 0);
    state->cc.s = (0x80 == (state->a & 0x80));
    state->cc.p = parity(state->a, 8);
}

void unimplemented_instruction(State8080* state) {
    g_printerr("\nerror: Unimplemented instruction:\n");
    state->pc--;
    dissassemble_8080_opcode(state->memory, state->pc);
    g_print("\n");
    exit(1);
}

bool emulate_8080_op(State8080* state) {
    int cycles = 0;

    unsigned char *opcode = &state->memory[state->pc];

    // dissassemble_8080_opcode(state->memory, state->pc);

    state->pc+=1;

    switch(*opcode) {
        case 0x00: cycles = 1; break; // NOP
        case 0x01: { // LXI B,D16
            state->c = opcode[1];
            state->b = opcode[2];
            state->pc += 2;
            cycles = 3;
        } break;
        case 0x02: unimplemented_instruction(state); break;
        case 0x03: unimplemented_instruction(state); break;
        case 0x04: unimplemented_instruction(state); break;
        case 0x05: { // DCR B
            uint8_t res = state->b - 1;
            state->cc.z = (res == 0);
            state->cc.s = (0x80 == (res & 0x80));
            state->cc.p = parity(res, 8);
            state->b = res;
            cycles = 1;
        } break;
        case 0x06: { // MVI B,byte
            state->b = opcode[1];
            state->pc++;
            cycles = 2;
        } break;
        case 0x07: unimplemented_instruction(state); break;
        case 0x08: unimplemented_instruction(state); break;
        case 0x09: { // DAD B
            uint32_t hl = (state->h << 8) | state->l;
            uint32_t bc = (state->b << 8) | state->c;
            uint32_t res = hl + bc;
            state->h = (res & 0xff00) >> 8;
            state->l = res & 0xff;
            state->cc.cy = ((res & 0xffff0000) != 0);
            cycles = 3;
        } break;
        case 0x0a: unimplemented_instruction(state); break;
        case 0x0b: unimplemented_instruction(state); break;
        case 0x0c: unimplemented_instruction(state); break;
        case 0x0d: { // DCR C
            uint8_t res = state->c - 1;
            state->cc.z = (res == 0);
            state->cc.s = (0x80 == (res & 0x80));
            state->cc.p = parity(res, 8);
            state->c = res;
            cycles = 1;
        } break;
        case 0x0e: { // MVI C,byte
            state->c = opcode[1];
            state->pc++;
            cycles = 2;
        } break;
        case 0x0f: { // RRC
            uint8_t x = state->a;
            state->a = ((x & 1) << 7) | (x >> 1);
            state->cc.cy = (1 == (x & 1));
            cycles = 1;
        } break;
        case 0x10: unimplemented_instruction(state); break;
        case 0x11: { // LXI D,D16
            state->e = opcode[1];
            state->d = opcode[2];
            state->pc += 2;
            cycles = 3;
        } break;
        case 0x12: unimplemented_instruction(state); break;
        case 0x13: { // INX D
            state->e++;
            if (state->e == 0) {
                state->d++;
            }
            cycles = 1;
        } break;
        case 0x14: unimplemented_instruction(state); break;
        case 0x15: unimplemented_instruction(state); break;
        case 0x16: unimplemented_instruction(state); break;
        case 0x17: unimplemented_instruction(state); break;
        case 0x18: unimplemented_instruction(state); break;
        case 0x19: { // DAD D
            uint32_t hl = (state->h << 8) | state->l;
            uint32_t de = (state->d << 8) | state->e;
            uint32_t res = hl + de;
            state->h = (res & 0xff00) >> 8;
            state->l = res & 0xff;
            state->cc.cy = ((res & 0xffff0000) != 0);
            cycles = 3;
        } break;
        case 0x1a: { // LDAX D
            uint16_t offset = (state->d << 8) | state->e;
            state->a = state->memory[offset];
            cycles = 2;
        } break;
        case 0x1b: unimplemented_instruction(state); break;
        case 0x1c: unimplemented_instruction(state); break;
        case 0x1d: unimplemented_instruction(state); break;
        case 0x1e: unimplemented_instruction(state); break;
        case 0x1f: unimplemented_instruction(state); break;
        case 0x20: unimplemented_instruction(state); break;
        case 0x21: { // LXI H,D16
            state->l = opcode[1];
            state->h = opcode[2];
            state->pc += 2;
            cycles = 3;
        } break;
        case 0x22: unimplemented_instruction(state); break;
        case 0x23: { // INX H
            state->l++;
            if (state->l == 0) {
                state->h++;
            }
            cycles = 1;
        } break;
        case 0x24: unimplemented_instruction(state); break;
        case 0x25: unimplemented_instruction(state); break;
        case 0x26: { // MVI H,byte
            state->h = opcode[1];
            state->pc++;
            cycles = 3;
        } break;
        case 0x27: unimplemented_instruction(state); break;
        case 0x28: unimplemented_instruction(state); break;
        case 0x29: { // DAD H
            uint32_t hl = (state->h << 8) | state->l;
            uint32_t res = hl + hl;
            state->h = (res & 0xff00) >> 8;
            state->l = res & 0xff;
            state->cc.cy = ((res & 0xffff0000) != 0);
            cycles = 3;
        } break;
        case 0x2a: unimplemented_instruction(state); break;
        case 0x2b: unimplemented_instruction(state); break;
        case 0x2c: unimplemented_instruction(state); break;
        case 0x2d: unimplemented_instruction(state); break;
        case 0x2e: unimplemented_instruction(state); break;
        case 0x2f: unimplemented_instruction(state); break;
        case 0x30: unimplemented_instruction(state); break;
        case 0x31: { //LXI SP,word
            state->sp = (opcode[2] << 8) | opcode[1];
            state->pc += 2;
            cycles = 3;
        } break;
        case 0x32: { // STA adr
            uint16_t offset = (opcode[2] << 8) | opcode[1];
            state->memory[offset] = state->a;
            state->pc += 2;
            cycles = 4;
        } break;
        case 0x33: unimplemented_instruction(state); break;
        case 0x34: unimplemented_instruction(state); break;
        case 0x35: unimplemented_instruction(state); break;
        case 0x36: { // MVI M,byte
            uint16_t offset = (state->h << 8) | state->l;
            state->memory[offset] = opcode[1];
            state->pc++;
            cycles = 3;
        } break;
        case 0x37: unimplemented_instruction(state); break;
        case 0x38: unimplemented_instruction(state); break;
        case 0x39: unimplemented_instruction(state); break;
        case 0x3a: { // LDA adr
            uint16_t offset = (opcode[2] << 8) | opcode[1];
            state->a = state->memory[offset];
            state->pc += 2;
            cycles = 4;
        } break;
        case 0x3b: unimplemented_instruction(state); break;
        case 0x3c: unimplemented_instruction(state); break;
        case 0x3d: unimplemented_instruction(state); break;
        case 0x3e: { // MVI A,byte
            state->a = opcode[1];
            state->pc++;
            cycles = 2;
        } break;
        case 0x3f: unimplemented_instruction(state); break;
        case 0x40: unimplemented_instruction(state); break;
        case 0x41: unimplemented_instruction(state); break;
        case 0x42: unimplemented_instruction(state); break;
        case 0x43: unimplemented_instruction(state); break;
        case 0x44: unimplemented_instruction(state); break;
        case 0x45: unimplemented_instruction(state); break;
        case 0x46: unimplemented_instruction(state); break;
        case 0x47: unimplemented_instruction(state); break;
        case 0x48: unimplemented_instruction(state); break;
        case 0x49: unimplemented_instruction(state); break;
        case 0x4a: unimplemented_instruction(state); break;
        case 0x4b: unimplemented_instruction(state); break;
        case 0x4c: unimplemented_instruction(state); break;
        case 0x4d: unimplemented_instruction(state); break;
        case 0x4e: unimplemented_instruction(state); break;
        case 0x4f: unimplemented_instruction(state); break;
        case 0x50: unimplemented_instruction(state); break;
        case 0x51: unimplemented_instruction(state); break;
        case 0x52: unimplemented_instruction(state); break;
        case 0x53: unimplemented_instruction(state); break;
        case 0x54: unimplemented_instruction(state); break;
        case 0x55: unimplemented_instruction(state); break;
        case 0x56: { // MOV D,M
            uint16_t offset = (state->h << 8) | state->l;
            state->d = state->memory[offset];
        } break;
        case 0x57: unimplemented_instruction(state); break;
        case 0x58: unimplemented_instruction(state); break;
        case 0x59: unimplemented_instruction(state); break;
        case 0x5a: unimplemented_instruction(state); break;
        case 0x5b: unimplemented_instruction(state); break;
        case 0x5c: unimplemented_instruction(state); break;
        case 0x5d: unimplemented_instruction(state); break;
        case 0x5e: { // MOV E,M
            uint16_t offset = (state->h << 8) | state->l;
            state->e = state->memory[offset];
            cycles = 2;
        } break;
        case 0x5f: unimplemented_instruction(state); break;
        case 0x60: unimplemented_instruction(state); break;
        case 0x61: unimplemented_instruction(state); break;
        case 0x62: unimplemented_instruction(state); break;
        case 0x63: unimplemented_instruction(state); break;
        case 0x64: unimplemented_instruction(state); break;
        case 0x65: unimplemented_instruction(state); break;
        case 0x66: { // MOV H,M
            uint16_t offset = (state->h << 8) | state->l;
            state->h = state->memory[offset];
            cycles = 2;
        } break;
        case 0x67: unimplemented_instruction(state); break;
        case 0x68: unimplemented_instruction(state); break;
        case 0x69: unimplemented_instruction(state); break;
        case 0x6a: unimplemented_instruction(state); break;
        case 0x6b: unimplemented_instruction(state); break;
        case 0x6c: unimplemented_instruction(state); break;
        case 0x6d: unimplemented_instruction(state); break;
        case 0x6e: unimplemented_instruction(state); break;
        case 0x6f: { // MOV L,A
            state->l = state->a;
            cycles = 1;
        } break;
        case 0x70: unimplemented_instruction(state); break;
        case 0x71: unimplemented_instruction(state); break;
        case 0x72: unimplemented_instruction(state); break;
        case 0x73: unimplemented_instruction(state); break;
        case 0x74: unimplemented_instruction(state); break;
        case 0x75: unimplemented_instruction(state); break;
        case 0x76: unimplemented_instruction(state); break;
        case 0x77: { // MOV M,A
            uint16_t offset = (state->h << 8) | state->l;
            state->memory[offset] = state->a;
            cycles = 2;
        } break;
        case 0x78: unimplemented_instruction(state); break;
        case 0x79: unimplemented_instruction(state); break;
        case 0x7a: { // MOV A,D
            state->a = state->d;
            cycles = 1;
        } break;
        case 0x7b: { // MOV A,E
            state->a = state->e;
            cycles = 1;
        } break;
        case 0x7c: { // MOV A,H
            state->a = state->h;
            cycles = 1;
        } break;
        case 0x7d: unimplemented_instruction(state); break;
        case 0x7e: { // MOV A,M
            uint16_t offset = (state->h << 8) | state->l;
            state->a = state->memory[offset];
            cycles = 2;
        } break;
        case 0x7f: unimplemented_instruction(state); break;
        case 0x80: unimplemented_instruction(state); break;
        case 0x81: unimplemented_instruction(state); break;
        case 0x82: unimplemented_instruction(state); break;
        case 0x83: unimplemented_instruction(state); break;
        case 0x84: unimplemented_instruction(state); break;
        case 0x85: unimplemented_instruction(state); break;
        case 0x86: unimplemented_instruction(state); break;
        case 0x87: unimplemented_instruction(state); break;
        case 0x88: unimplemented_instruction(state); break;
        case 0x89: unimplemented_instruction(state); break;
        case 0x8a: unimplemented_instruction(state); break;
        case 0x8b: unimplemented_instruction(state); break;
        case 0x8c: unimplemented_instruction(state); break;
        case 0x8d: unimplemented_instruction(state); break;
        case 0x8e: unimplemented_instruction(state); break;
        case 0x8f: unimplemented_instruction(state); break;
        case 0x90: unimplemented_instruction(state); break;
        case 0x91: unimplemented_instruction(state); break;
        case 0x92: unimplemented_instruction(state); break;
        case 0x93: unimplemented_instruction(state); break;
        case 0x94: unimplemented_instruction(state); break;
        case 0x95: unimplemented_instruction(state); break;
        case 0x96: unimplemented_instruction(state); break;
        case 0x97: unimplemented_instruction(state); break;
        case 0x98: unimplemented_instruction(state); break;
        case 0x99: unimplemented_instruction(state); break;
        case 0x9a: unimplemented_instruction(state); break;
        case 0x9b: unimplemented_instruction(state); break;
        case 0x9c: unimplemented_instruction(state); break;
        case 0x9d: unimplemented_instruction(state); break;
        case 0x9e: unimplemented_instruction(state); break;
        case 0x9f: unimplemented_instruction(state); break;
        case 0xa0: unimplemented_instruction(state); break;
        case 0xa1: unimplemented_instruction(state); break;
        case 0xa2: unimplemented_instruction(state); break;
        case 0xa3: unimplemented_instruction(state); break;
        case 0xa4: unimplemented_instruction(state); break;
        case 0xa5: unimplemented_instruction(state); break;
        case 0xa6: unimplemented_instruction(state); break;
        case 0xa7: { // ANA A
            state->a = state->a & state->a;
            logic_flags_a(state);
            cycles = 1;
        } break;
        case 0xa8: unimplemented_instruction(state); break;
        case 0xa9: unimplemented_instruction(state); break;
        case 0xaa: unimplemented_instruction(state); break;
        case 0xab: unimplemented_instruction(state); break;
        case 0xac: unimplemented_instruction(state); break;
        case 0xad: unimplemented_instruction(state); break;
        case 0xae: unimplemented_instruction(state); break;
        case 0xaf: { // XRA A
            state->a = state->a ^ state->a;
            logic_flags_a(state);
            cycles = 1;
        } break;
        case 0xb0: unimplemented_instruction(state); break;
        case 0xb1: unimplemented_instruction(state); break;
        case 0xb2: unimplemented_instruction(state); break;
        case 0xb3: unimplemented_instruction(state); break;
        case 0xb4: unimplemented_instruction(state); break;
        case 0xb5: unimplemented_instruction(state); break;
        case 0xb6: unimplemented_instruction(state); break;
        case 0xb7: unimplemented_instruction(state); break;
        case 0xb8: unimplemented_instruction(state); break;
        case 0xb9: unimplemented_instruction(state); break;
        case 0xba: unimplemented_instruction(state); break;
        case 0xbb: unimplemented_instruction(state); break;
        case 0xbc: unimplemented_instruction(state); break;
        case 0xbd: unimplemented_instruction(state); break;
        case 0xbe: unimplemented_instruction(state); break;
        case 0xbf: unimplemented_instruction(state); break;
        case 0xc0: unimplemented_instruction(state); break;
        case 0xc1: { // POP B
            state->c = state->memory[state->sp];
            state->b = state->memory[state->sp+1];
            state->sp+=2;
            cycles = 3;
        } break;
        case 0xc2: { //JNZ
            if (state->cc.z == 0) {
                state->pc = (opcode[2] << 8) | opcode[1];
            } else {
                state->pc += 2;
            }
            cycles = 3; // ???
        } break;
        case 0xc3: { //JMP adr
            state->pc = (opcode[2] << 8) | opcode[1];
            cycles = 3;
        } break;
        case 0xc4: unimplemented_instruction(state); break;
        case 0xc5: { // PUSH B
            state->memory[state->sp-1] = state->b;
            state->memory[state->sp-2] = state->c;
            state->sp = state->sp - 2;
            cycles = 3;
        } break;
        case 0xc6: { // ADI byte
            uint16_t res = (uint16_t) state->a + (uint16_t) opcode[1];
            state->cc.z = ((res & 0xff) == 0);
            state->cc.s = (0x80 == (res & 0x80));
            state->cc.p = parity((res & 0xff), 8);
            state->cc.cy = (res > 0xff);
            state->a = (uint8_t) res;
            state->pc++;
            cycles = 2;
        } break;
        case 0xc7: unimplemented_instruction(state); break;
        case 0xc8: unimplemented_instruction(state); break;
        case 0xc9: { // RET
            state->pc = state->memory[state->sp] | (state->memory[state->sp+1] << 8);
            state->sp += 2;
            cycles = 3;
        } break;
        case 0xca: unimplemented_instruction(state); break;
        case 0xcb: unimplemented_instruction(state); break;
        case 0xcc: unimplemented_instruction(state); break;
        case 0xcd: { //CALL adr
            uint16_t ret = state->pc+2;
            state->memory[state->sp-1] = (ret >> 8) & 0xff;
            state->memory[state->sp-2] = (ret & 0xff);
            state->sp = state->sp - 2;
            state->pc = (opcode[2] << 8) | opcode[1];
            cycles = 5;
        } break;
        case 0xce: unimplemented_instruction(state); break;
        case 0xcf: unimplemented_instruction(state); break;
        case 0xd0: unimplemented_instruction(state); break;
        case 0xd1: { // POP D
            state->e = state->memory[state->sp];
            state->d = state->memory[state->sp+1];
            state->sp+=2;
            cycles = 3;
        } break;
        case 0xd2: unimplemented_instruction(state); break;
        case 0xd3: { // OUT
            uint8_t port = opcode[1];
            machine_out(state, port);
            state->pc++;
            cycles = 3;
        } break;
        case 0xd4: unimplemented_instruction(state); break;
        case 0xd5: { // PUSH D
            state->memory[state->sp-1] = state->d;
            state->memory[state->sp-2] = state->e;
            state->sp = state->sp - 2;
            cycles = 3;
        } break;
        case 0xd6: unimplemented_instruction(state); break;
        case 0xd7: unimplemented_instruction(state); break;
        case 0xd8: unimplemented_instruction(state); break;
        case 0xd9: unimplemented_instruction(state); break;
        case 0xda: unimplemented_instruction(state); break;
        case 0xdb: { // IN
            uint8_t port = opcode[1];
            state->a = machine_in(state, port);
            state->pc++;
            cycles = 3;
        } break;
        case 0xdc: unimplemented_instruction(state); break;
        case 0xdd: unimplemented_instruction(state); break;
        case 0xde: unimplemented_instruction(state); break;
        case 0xdf: unimplemented_instruction(state); break;
        case 0xe0: unimplemented_instruction(state); break;
        case 0xe1: { // POP H
            state->l = state->memory[state->sp];
            state->h = state->memory[state->sp+1];
            state->sp+=2;
            cycles = 3;
        } break;
        case 0xe2: unimplemented_instruction(state); break;
        case 0xe3: unimplemented_instruction(state); break;
        case 0xe4: unimplemented_instruction(state); break;
        case 0xe5: { // PUSH H
            state->memory[state->sp-1] = state->h;
            state->memory[state->sp-2] = state->l;
            state->sp = state->sp - 2;
            cycles = 3;
        } break;
        case 0xe6: { // ANI,byte
            state->a = state->a & opcode[1];
            logic_flags_a(state);
            state->pc++;
            cycles = 2;
        } break;
        case 0xe7: unimplemented_instruction(state); break;
        case 0xe8: unimplemented_instruction(state); break;
        case 0xe9: unimplemented_instruction(state); break;
        case 0xea: unimplemented_instruction(state); break;
        case 0xeb: { // XCHG
            uint8_t tmp = state->d;
            uint8_t tmp2 = state->e;
            state->d = state->h;
            state->e = state->l;
            state->h = tmp;
            state->l = tmp2;
            cycles = 1;
        } break;
        case 0xec: unimplemented_instruction(state); break;
        case 0xed: unimplemented_instruction(state); break;
        case 0xee: unimplemented_instruction(state); break;
        case 0xef: unimplemented_instruction(state); break;
        case 0xf0: unimplemented_instruction(state); break;
        case 0xf1: { // POP PSW
            state->a = state->memory[state->sp+1];
            uint8_t psw = state->memory[state->sp];
            state->cc.z = (0x01 == (psw & 0x01));
            state->cc.s = (0x02 == (psw & 0x02));
            state->cc.p = (0x03 == (psw & 0x03));
            state->cc.cy = (0x04 == (psw & 0x04));
            state->cc.ac = (0x05 == (psw & 0x05));
            state->sp += 2;
            cycles = 3;
        } break;
        case 0xf2: unimplemented_instruction(state); break;
        case 0xf3: unimplemented_instruction(state); break;
        case 0xf4: unimplemented_instruction(state); break;
        case 0xf5: { // PUSH PSW
            state->memory[state->sp-1] = state->a;
            uint8_t psw = (state->cc.z |
                           state->cc.s << 1 |
                           state->cc.p << 2 |
                           state->cc.cy << 3 |
                           state->cc.ac << 4);
            state->memory[state->sp-2] = psw;
            state->sp = state->sp - 2;
            cycles = 3;
        } break;
        case 0xf6: unimplemented_instruction(state); break;
        case 0xf7: unimplemented_instruction(state); break;
        case 0xf8: unimplemented_instruction(state); break;
        case 0xf9: unimplemented_instruction(state); break;
        case 0xfa: unimplemented_instruction(state); break;
        case 0xfb: { // EI
            state->int_enable = 1; // enable interrupts
        } break;
        case 0xfc: unimplemented_instruction(state); break;
        case 0xfd: unimplemented_instruction(state); break;
        case 0xfe: { // CPI D8
            uint8_t x = state->a - opcode[1];
            state->cc.z = (x == 0);
            state->cc.s = (0x80 == (x & 0x80));
            state->cc.p = parity(x, 8);
            state->cc.cy = (state->a < opcode [1]);
            state->pc++;
            cycles = 2;
        } break;
        case 0xff: unimplemented_instruction(state); break;
    }
    // g_print("\t");
    // g_print("%c", state->cc.z ? 'z' : '.');
    // g_print("%c", state->cc.s ? 's' : '.');
    // g_print("%c", state->cc.p ? 'p' : '.');
    // g_print("%c", state->cc.cy ? 'c' : '.');
    // g_print("%c  ", state->cc.ac ? 'a' : '.');
    // g_print("A $%02x B $%02x C $%02x D $%02x E $%02x H $%02x L $%02x SP %04x\n",
    //        state->a, state->b, state->c, state->d,
    //        state->e, state->h, state->l, state->sp);

    sleep_cycles(cycles);
    return true;
}
