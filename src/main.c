#include <gtk/gtk.h>

#include "eightyeighty.h"

int main(int argc, char *argv[]) {
    g_application_run(G_APPLICATION (eightyeighty_app_new()), argc, argv);
}
