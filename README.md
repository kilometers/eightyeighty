# EightyEighty

An intel 8080 emulator in C. This probably isn't ideal code, it was written for educational purposes.

## Dependencies

```
sudo dnf install gtk3-devel
```

## Building

```
git clone https://gitlab.com/kilometers/eightyeighty
cd eightyeighty
meson . build
ninja -C build
sudo ninja -C build install
```